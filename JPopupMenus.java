import java.awt.BorderLayout;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.Color; 
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener;
import javax.swing.JButton; 
import javax.swing.JFrame; 
import javax.swing.JColorChooser; 
import javax.swing.JPanel;

public class JPopupMenus extends JFrame implements ActionListener {
	private Color color = Color.LIGHT_GRAY;
	private JPanel panel1, panel2;
	private JRadioButton green, orange, blue;
	
	public JPopupMenus() {
		green = new JRadioButton("GREEN");
		orange = new JRadioButton("ORANGE");
		blue = new JRadioButton("BLUE");
		
		panel1 = new JPanel();
		panel2 = new JPanel();
		panel1.add(green);
		panel1.add(orange);
		panel1.add(blue);
		getContentPane().add(panel2, BorderLayout.CENTER);
		panel2.setSize(200, 200);
		getContentPane().add(panel1, BorderLayout.NORTH);
		
		ButtonGroup group = new ButtonGroup();
		group.add(blue);
		group.add(orange);
		group.add(green);
		
		green.addActionListener(this);
		orange.addActionListener(this);
		blue.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource() == blue)
			panel2.setBackground(Color.BLUE);
		if(ae.getSource() == orange)
			panel2.setBackground(Color.ORANGE);
		if(ae.getSource() == green)
			panel2.setBackground(Color.GREEN);
	}
	
	public static void main(String[] args) {
		JPopupMenus frame = new JPopupMenus();
		frame.setTitle("Using JPopupMenus");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(600, 400);
		frame.setVisible(true);
	}
}
