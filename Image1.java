import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Image1 extends JPanel
{
    public void paintComponent(Graphics objek){
        int[] x = {50,50,90,90,150,90,90};
        int[] y = {55,85,85,110,70,30,55};
        super.paintComponent(objek);
        this.setBackground(Color.white);
        objek.setColor(new Color(255,255,0));
        objek.fillArc(15, 200, 100, 100, 10, 340);
        objek.setColor(new Color(255,0,0));
        objek.fillRect(250, 55, 100, 75);
        objek.setColor(new Color(0,255,255));
        objek.fillPolygon(x, y, 7);
        objek.setColor(new Color(0,255,0));
        objek.fillOval(250, 200, 100, 75);

    }

    public static void main(String[] args){
        JFrame frame = new JFrame("Try Graphic Image");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Image1 gambar = new Image1();
        frame.add(gambar);
        frame.setSize(450,400);
        frame.setVisible(true);
    }
}
