import java.lang.Integer;
import java.awt.*;
import java.awt.event.*;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.Font.*;
import java.awt.geom.*;
import java.awt.image.*;
import javax.swing.*;

public class StrokeAndFill extends JApplet implements ItemListener
{
    JLabel primlabel,linelabel,paintlabel,strokelabel;
    ShapePanel display;
    static JComboBox primitive,line,paint,stroke;
    int index = 0;
    public static boolean no2D = false;

    public void init(){
        GridBagLayout layout = new GridBagLayout();
        getContentPane().setLayout(layout);
        GridBagConstraints c = new GridBagConstraints();

        c.weightx = 1.0;
        c.fill = GridBagConstraints.BOTH;
        primlabel = new JLabel();
        primlabel.setText("Primitive");
        Font newFont = getFont().deriveFont(1);
        primlabel.setFont(newFont);
        primlabel.setHorizontalAlignment(JLabel.CENTER);
        layout.setConstraints(primlabel, c);
        getContentPane().add(primlabel);
        
        linelabel = new JLabel();
        linelabel.setText("Lines");
        linelabel.setFont(newFont);
        linelabel.setHorizontalAlignment(JLabel.CENTER);
        layout.setConstraints(linelabel,c);
        getContentPane().add(linelabel);

        c.gridwidth = GridBagConstraints.RELATIVE;
        paintlabel = new JLabel();
        paintlabel.setText("Paint");
        paintlabel.setFont(newFont);
        paintlabel.setHorizontalAlignment(JLabel.CENTER);
        layout.setConstraints(paintlabel, c);
        getContentPane().add(paintlabel);

        c.gridwidth = GridBagConstraints.REMAINDER;
        strokelabel = new JLabel();
        strokelabel.setText("Rendering");
        strokelabel.setFont(newFont);
        strokelabel.setHorizontalAlignment(JLabel.CENTER);
        layout.setConstraints(strokelabel, c);
        getContentPane().add(strokelabel);

        GridBagConstraints ls = new GridBagConstraints();
        ls.weightx = 1.0;
        ls.fill = GridBagConstraints.BOTH;

        primitive = new JComboBox(new Object[] {
                            "rectangle", "elips", "text"
                        });
        primitive.addItemListener(this);
        newFont = newFont.deriveFont(0,14.0f);
        primitive.setFont(newFont);
        layout.setConstraints(primitive, ls);     
        getContentPane().add(primitive);

        line = new JComboBox(new Object[] {"thin","thick","dash"});
        line.addItemListener(this);
        layout.setConstraints(line, ls);
        getContentPane().add(line);

        ls.gridwidth = GridBagConstraints.RELATIVE;
        paint = new JComboBox(new Object[]{"solid","gradient","polka"});
        paint.addItemListener(this);
        layout.setConstraints(paint, c);
        getContentPane().add(paint);

        ls.gridwidth = GridBagConstraints.REMAINDER;
        stroke = new JComboBox(new Object[] {"Stroke","Fill","Stroke & Feel"});
        stroke.addItemListener(this);
        stroke.setFont(newFont);
        layout.setConstraints(stroke, ls);
        getContentPane().add(stroke);
        
        GridBagConstraints sc = new GridBagConstraints();
        sc.fill = GridBagConstraints.BOTH;
        sc.weightx = 1.0;
        sc.weighty = 1.0;
        sc.gridwidth = GridBagConstraints.REMAINDER;
        display = new ShapePanel();
        layout.setConstraints(display, sc);
        display.setBackground(Color.white);
        getContentPane().add(display);

        validate();
    }

    public void itemStateChanged(ItemEvent e){
        
    }

    public static void main(String[] argv){
        if(argv.length>0 && argv[0].equals("-no2d")){
            StrokeAndFill.no2D = true;

        }

        JFrame frame = new JFrame("Stroke And Fill");
        frame.addWindowFocusListener(new WindowAdapter() {
            public void windiwClosing(WindowEvent e){
                System.exit(0);
            }
        });

        JApplet applet = new StrokeAndFill();
        frame.getContentPane().add(BorderLayout.CENTER, applet);
        applet.init();
        frame.setSize(450,250);
        frame.show();
    }
}

class ShapePanel extends JPanel
{
    BasicStroke bstroke = new BasicStroke(3.0f);
    int w,h;
    Shape shapes[] = new Shape[3];

    public ShapePanel(){
        setBackground((Color.white));
        shapes[0] = new Rectangle(0,0,100,100);
        shapes[1] = new Ellipse2D.Double(0.0,0.0,100.0,100.0);
        TextLayout textTl = new TextLayout("Text",new Font("Helvatica",1,96),new FontRenderContext(null, false, false));
        AffineTransform textAt = new AffineTransform();
        textAt.translate(0, (float)textTl.getBounds().getHeight());
        shapes[2] = textTl.getOutline(textAt);

    }
    // Invokes the paint method
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        if(!StrokeAndFill.no2D){
            Graphics2D g2 = (Graphics2D) g;
            Dimension d = getSize();
            w = d.width;
            h = d.height;
            int width,height;

            //print the initial instruction
            String instruct = "pick a primitive, Line style,Paint, and Rendering Method";
            TextLayout thisTl = new TextLayout(instruct, new Font("Helvetica",0,10),g2.getFontRenderContext());
            Rectangle2D bounds = thisTl.getBounds();
            width = (int)bounds.getWidth();
            thisTl.draw(g2,w/2-width/2,20);

            Stroke oldStroke = g2.getStroke();

            //sets the stroke
            switch (StrokeAndFill.line.getSelectedIndex()){
                case 0 : g2.setStroke(new BasicStroke(3.0f)); break;
                case 1 : g2.setStroke(new BasicStroke(8.0f)); break;
                case 2 : float dash[] = {10.0f};
                    g2.setStroke(new BasicStroke(3.0f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER,10.0f,dash,0.0f));
                    break;
            }

            Paint oldPaint = g2.getPaint();

            //set the Paint
            switch (StrokeAndFill.paint.getSelectedIndex()){
                case 0 : g2.setPaint(Color.BLUE); break;
                case 1 : g2.setPaint(new GradientPaint(0, 0, Color.lightGray, w-25,h,Color.blue,false)); break;
                case 2 : BufferedImage bi = new BufferedImage(5,5,BufferedImage.TYPE_INT_RGB);
                    Graphics2D big = bi.createGraphics();
                    big.setColor(Color.blue);
                    big.fillRect(0, 0, 5, 5);
                    big.setColor(Color.lightGray);
                    big.fillOval(0, 0, 5, 5);
                    Rectangle r = new Rectangle(0,0,5,5);
                    g2.setPaint(new TexturePaint(bi,r));
                    break;
            }

            //set the shape
            Shape shape = shapes[StrokeAndFill.primitive.getSelectedIndex()];
            Rectangle r = shape.getBounds();

            //Set the selected shape to the center of canvas
            AffineTransform saveXform = g2.getTransform();
            AffineTransform toCenterAt = new AffineTransform();
            toCenterAt.translate(w/2-(r.width/2),h/2-(r.height/2));
            g2.transform(toCenterAt);

            //Determine wheather to fill 
            switch (StrokeAndFill.stroke.getSelectedIndex()){
                case 0 : g2.draw(shape); break;
                case 1 : g2.fill(shape); break;
                case 2 : Graphics2D tempg2 = g2;
                    g2.fill(shape);
                    g2.setColor(Color.darkGray);
                    g2.draw(shape);
                    g2.setPaint(tempg2.getPaint()); break;
            }
            g2.setTransform(saveXform);
            g2.setStroke(oldStroke);
            g2.setPaint(oldPaint);
        }else{
            g.drawRect(0, 0, 100, 100);
        }
    }
}
