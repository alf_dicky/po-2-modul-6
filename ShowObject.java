import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ShowObject extends JPanel {
	public void Component(Graphics g) {
		super.Component(g);
		this.setBackground(Color.WHITE);
		
		Color colorRect = Color.BLUE;
		Color colorOval = Color.GREEN;
		Color colorPolygon = Color.RED;
		Color colorArc = Color.YELLOW;
		Color colorText = Color.BLACK;
		
		//Draw Arc
		g.drawString("Arc ", 15, 70);
		g.setColor(colorArc);
		g.fillArc(15, 25, 60, 60, 0, 90);
		g.setColor(colorText);
		
		//Draw Oval
		g.drawString("Oval ", 15, 140);
		g.setColor(colorOval);
		g.fillOval(15, 90, 90, 30);
		g.setColor(colorText);
		
		//Draw Polygon
		g.drawString("Polygon ", 15, 230);
		g.setColor(colorPolygon);
		int[] xPoints = {55, 15, 35, 75, 95};
		int[] yPoints = {150, 180, 210, 210, 180};
		int nPoints = 5;
		g.fillPolygon(xPoints, yPoints, nPoints);
		g.setColor(colorText);
		
		//Draw Rectangle
		g.drawString("Rectangle ", 15, 300);
		g.setColor(colorRect);
		g.fillRect(15, 250, 60, 30);
		g.setColor(colorText);
		
	}
		
	public static void main(String[] args) {
		JFrame callFrame = new JFrame("Drawing Object at JFrame");
		callFrame.setSize(150, 350);
		callFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		callFrame.setContentPane(new ShowObject());
		callFrame.setVisible(true);
	}
}
