import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.BasicStroke;
import java.awt.geom.*;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Image2 extends JPanel
{
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        this.setBackground(Color.WHITE);
        Graphics2D g2 = (Graphics2D) g;

        //Draw Line
        g2.setPaint(Color.blue);
        g2.setStroke(new BasicStroke(5.0f));
        g2.draw(new Line2D.Double(30,70,100,15));
        //draw rectangle
        g2.draw(new Rectangle2D.Double(100, 75, 100, 50));

        //Draw RoundRectangle
        Stroke dashed = new BasicStroke(3,BasicStroke.CAP_BUTT,BasicStroke.JOIN_BEVEL,0,new float[]{9},0);
        g2.setStroke(dashed);
        g2.draw(new RoundRectangle2D.Double(50.0, 150.0, 200.0, 75.0, 50, 50));

        //draw Arc2D
        g2.setStroke(new BasicStroke(8.0f));
        g2.draw(new Arc2D.Double(50,300,150,100,90,135,Arc2D.OPEN));
        

    }

    public static void main(String[] args){
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Image2 gambar2 = new Image2();
        frame.add(gambar2);
        frame.setSize(300,500);
        frame.setVisible(true);
    }
}
