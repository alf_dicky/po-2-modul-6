import java.awt.Graphics;
import java.awt.Color;
import javax.swing.*;

public class ColorJPanel extends JPanel{
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        this.setBackground(Color.WHITE);
        g.setColor(new Color(255, 0, 0));
        g.fillRect(15, 25, 100, 20);
        g.drawString("Current RGB : "+ g.getColor(), 130, 40);
        
        g.setColor(new Color(0.50f, 0.75f, 0.0f));
        g.fillRect(15, 50, 100, 20);
        g.drawString("Current RGB: "+ g.getColor(), 130, 65);
        
        g.setColor(Color.BLUE);
        g.fillRect(15, 75, 100, 20);
        g.drawString("Current RGB : "+ g.getColor(), 130, 90);
        
        g.setColor(Color.ORANGE);
        g.fillRect(15, 100, 100, 20);
        g.drawString("Current RGB : "+ g.getColor(), 130, 115);
        
        g.setColor(Color.YELLOW);
        g.fillRect(15, 125, 100, 20);
        g.drawString("Current RGB : "+ g.getColor(), 130, 140);

        g.setColor(Color.GREEN);
        g.fillRect(15, 150, 100, 20);
        g.drawString("Current RGB : "+ g.getColor(), 130, 165);
        
        g.setColor(new Color(120, 120, 120));
        g.fillRect(15, 175, 100, 20);
        g.drawString("Current RGB : "+ g.getColor(), 130, 190);
        
        g.setColor(new Color(128, 0, 0));
        g.fillRect(15, 200, 100, 20);
        g.drawString("Current RGB : "+ g.getColor(), 130, 215);
        
        g.setColor(new Color(0, 255, 255));
        g.fillRect(15, 225, 100, 20);
        g.drawString("Current RGB : "+ g.getColor(), 130, 240); 
		
		g.setColor(new Color(0, 191, 255));
        g.fillRect(15, 250, 100, 20);
        g.drawString("Current RGB : "+ g.getColor(), 130, 265); 

        Color color = Color.MAGENTA;
        g.setColor(color);
        g.fillRect(15, 275, 100, 20);
        g.drawString("RGB Values : "+ color.getRed()+ ", " +color.getGreen()+ ", " +color.getBlue(), 130, 290);
    }
}
